import pygame

pygame.init()

screen = pygame.display.set_mode((800, 600))
icon = pygame.image.load("brick-breaker.png")
player1 = pygame.image.load("PADDLE1.BMP")
player1x = 370
player1y = 480
bg = (0, 0, 0)

pygame.display.set_caption("Break-Thru!")
pygame.display.set_icon(icon)

def player():
    screen.blit(player1, (player1x, player1y))

running = True
while running:
    screen.fill(bg)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

    player()
    pygame.display.update()